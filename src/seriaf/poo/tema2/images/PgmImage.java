/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema2.images;

import java.awt.Dimension;
import javax.swing.ImageIcon;

/**
 *
 * @author rhobincu
 */
public interface PgmImage {

    /**
     * Converteste aceasta imagine intr-un obiect de tip ImageIcon pentru a putea fi afisat intr-o fereastra. Aceasta
     * metoda e deja implementata in clasa PgmImageBase si nu trebuie modificata.
     *
     * @return un obiect de tip ImageIcon
     */
    ImageIcon toImageIcon();

    /**
     * Intoarce dimensiunea imaginii ca obiect de tip java.awt.Dimension. Aceasta metoda e deja implementata in clasa
     * PgmImageBase si nu trebuie modificata.
     *
     * @return dimensiunea imaginii.
     */
    Dimension getImageSize();

    /**
     * Aplica un filtru trece jos cu o fereastra de size x size (size impar). Aplicarea filtrului implica ca pentru
     * fiecare pixel din imagine sa se faca media pixelilor dintr-o fereastra de size x size in jurul lui (cu el in
     * centru) si sa se inlocuiasca valoarea lui cu aceasta medie. La limita (colturi, margini), se replica valoarea din
     * margine/ colt pentru completarea ferestrei.
     *
     * @param size Dimensiunea ferestrei filtrului.
     */
    void applyLowPassFilter(int size);
}
