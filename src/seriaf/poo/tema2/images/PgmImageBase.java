/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema2.images;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;

/**
 *
 * @author rhobincu
 */
public abstract class PgmImageBase implements PgmImage {

    /**
     * Contine date despre imagine. Matricea e structurata sub forma mImageData[lungime][latime]. 
     * Fiecare element reprezinta o valoare de luminanta pentru un pixel in intervalul [0;255], desi
     * tipul de date byte in Java este cu semn [-128;127], deci atentie la cum se citesc datele din
     * aceasta matrice in momentul in care doriti sa aplicati filtrarea. Scrierea se face simplu,
     * convertind valoarea dorita la byte.
     * 
     * In fisierul PGM exista specificata valoarea maxima a unui pixel care va corespunde unei valori de 255
     * in aceasta matrice, deci trebuie facute calcule astfel incat intervalul de luminanta din fisier sa fie
     * expandat (sau comprimat) la intervalul [0-255].
     */
    protected byte mImageData[][];
    
    /**
     * Latimea imaginii, in pixeli (adica numarul de coloane).
     */
    protected int mWidth;
    
    /**
     * Lungimea imaginii, in pixeli (adica numarul de randuri).
     */
    protected int mHeight;

    @Override
    public ImageIcon toImageIcon() {
        BufferedImage image = new BufferedImage(mWidth, mHeight, BufferedImage.TYPE_BYTE_GRAY);

        for (int line = 0; line < mHeight; line++) {
            image.getRaster().setDataElements(0, line, mWidth, 1, mImageData[line]);
        }
        return new ImageIcon(image);
    }

    @Override
    public Dimension getImageSize() {
        return new Dimension(mWidth, mHeight);
    }

    @Override
    public void applyLowPassFilter(int size) {
        // Aceasta metoda trebuie suprascrisa in PgmImageImpl pentru a aplica imaginii un 
        // filtru trece-jos. Deocamdata nu face nimic. Aplicarea filtrului modifica datele din
        // mImageData
    }
}
