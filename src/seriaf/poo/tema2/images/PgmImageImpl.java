package seriaf.poo.tema2.images;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class PgmImageImpl extends PgmImageBase {

    BufferedReader br = null;

    void customReadPGM() {
        Scanner s = new Scanner(this.br);
        String magic = s.nextLine();
        int maxVal = 0;
        if ("P2".equals(magic)) {
            s.nextLine();
            mWidth = s.nextInt();
            mHeight = s.nextInt();
            mImageData = new byte[mHeight][mWidth];
            maxVal = s.nextInt();
            for (int ln = 0; ln < mHeight; ln++) {
                for (int col = 0; col < mWidth; col++) {
                    mImageData[ln][col] = (byte) (s.nextInt());
                }
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    public PgmImageImpl(String fn) throws IOException {
        this.br = new BufferedReader(new FileReader(fn));
        this.customReadPGM();
        br.close();
    }

    @Override
    public void applyLowPassFilter(int size) {
        if (size == 1 || size == 0) {
            throw new IllegalArgumentException();
        }
        for (int ln = 0; ln < mHeight; ln++) {
            for (int col = 0; col < mWidth; col++) {
                int el = Byte.toUnsignedInt(mImageData[ln][col]);
                int filtered = getFiltered(ln, col, size, el);
                mImageData[ln][col] = (byte) filtered;
            }
        }
    }

    public int getFiltered(int centerLn, int centerCol, int size, int centerEl) {
        int average = 0;
        int startLn = centerLn - ((size - 1) / 2);
        int startCol = centerCol - ((size - 1) / 2);
        for (int ln = startLn; ln < startLn + size; ln++) {
            for (int col = startCol; col < startCol + size; col++) {
                int el = getElement(ln, col, centerEl);
//                System.out.print("\t" + el);
                average += el;
            }
//            System.out.println();
        }
        average = (average) / (size * size);
        return average;
    }

    public int getElement(int ln, int col, int centerEl) {
        int toBeReturned = 0;
        if (ln >= mHeight) {
            ln = mHeight - 1;
        } else if (ln < 0) {
            ln = 0;
        }
        if (col >= mWidth) {
            col = mWidth - 1;
        } else if (col < 0) {
            col = 0;
        }

        try {
            toBeReturned = (int) Byte.toUnsignedInt(mImageData[ln][col]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(toBeReturned);
        }

        return toBeReturned;
    }
}
