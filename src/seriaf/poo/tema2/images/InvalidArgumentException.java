/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema2.images;

/**
 *
 * @author terec
 */
public class InvalidArgumentException extends Exception {

    public InvalidArgumentException(String errMsg) {
        super(errMsg);
    }
    
}
